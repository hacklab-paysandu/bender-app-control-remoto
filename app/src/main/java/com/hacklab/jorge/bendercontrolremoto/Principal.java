package com.hacklab.jorge.bendercontrolremoto;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Principal extends AppCompatActivity
{
    private static final String TAG = "BenderControl";
    private static final int VIBRACION = 1;

    private Vibrator vibrator;

    class BotonListener implements View.OnTouchListener
    {
        private static final String COMANDO_PARAR = "parar";
        private String comando;

        BotonListener ( String comando ) { this.comando = comando; }

        @Override
        @SuppressLint("ClickableViewAccessibility")
        public boolean onTouch ( View view, MotionEvent motionEvent )
        {
            switch ( motionEvent.getAction() )
            {
                case MotionEvent.ACTION_DOWN :
                    // Al presionar este view, se cambia el fondo para indicar que hay una acción
                    view.setBackgroundColor( view.getResources().getColor( R.color.colorPresionado ) );
                    // Se cambia el comando por el de este view
                    cambiarComando( this.comando );

                    // Se realiza un efecto de vibración
                    if ( vibrator != null )
                        vibrator.vibrate( VIBRACION );
                    return true;
                case MotionEvent.ACTION_UP:
                    // Al soltar este view, se coloca el fondo normal, para indicar que ya no esta activo
                    view.setBackgroundColor( view.getResources().getColor( R.color.colorNormal ) );
                    // Se cambia el comando por el de detener
                    cambiarComando( BotonListener.COMANDO_PARAR );
                    return true;
            }

            return false;
        }
    }

    private static List<TextView> textViews;

    class OpcionTextoListener implements View.OnTouchListener
    {
        private TextView tvNombre;
        private String comando;
        private int opcion;

        OpcionTextoListener ( TextView tvNombre, String comando, int opcion, boolean iniciarMarcado )
        {
            this.tvNombre = tvNombre;
            this.comando = comando;
            this.opcion = opcion;

            // Se inicializa el arreglo si no lo esta
            if ( textViews == null )
                textViews = new ArrayList<>();

            // Se agrega este nombre al arreglo, para luego poder desmarcarlos a todos
            textViews.add( tvNombre );

            // Indica si este elemento debe iniciar marcado o no
            if ( iniciarMarcado )
                marcar();
        }

        @Override
        @SuppressLint("ClickableViewAccessibility")
        public boolean onTouch ( View view, MotionEvent motionEvent )
        {
            switch ( motionEvent.getAction() )
            {
                case MotionEvent.ACTION_DOWN :
                    // Al presionar sobre el view, se desmarcan los nombres de todos los views
                    desmarcarNombres();
                    // Se marca el del presionado
                    marcar();

                    // Se realiza un efecto de vibración
                    if ( vibrator != null )
                        vibrator.vibrate( VIBRACION );
                    return true;
                case MotionEvent.ACTION_UP:
                    // Al soltar la view, se envía el comando
                    enviarComando( String.format( Locale.getDefault(), "%s:%d\n", comando, opcion ) );
                    return true;
            }

            return false;
        }

        private void desmarcarNombres ()
        {
            for ( TextView tv : textViews )
                tv.setBackgroundColor( tv.getResources().getColor( R.color.colorNormal ) );
        }

        private void marcar ()
        {
            tvNombre.setBackgroundColor( tvNombre.getResources().getColor( R.color.colorPresionado ) );
        }
    }

    @BindView( R.id.sbVelocidad )
    SeekBar sbVelocidad;

    @BindView( R.id.btnAdelante )
    ImageView btnAdelante;
    @BindView( R.id.btnAtras )
    ImageView btnAtras;
    @BindView( R.id.btnIzquierda )
    ImageView btnIzquierda;
    @BindView( R.id.btnDerecha )
    ImageView btnDerecha;

    @BindView( R.id.btnAdelanteDer )
    ImageView btnAdelanteDer;
    @BindView( R.id.btnAdelanteIzq )
    ImageView btnAdelanteIzq;
    @BindView( R.id.btnAtrasDer )
    ImageView btnAtrasDer;
    @BindView( R.id.btnAtrasIzq )
    ImageView btnAtrasIzq;

    @BindView( R.id.btnGirarIzq )
    ImageView btnGirarIzq;
    @BindView( R.id.btnGirarDer )
    ImageView btnGirarDer;

    @BindView( R.id.btnFeliz )
    ConstraintLayout btnFeliz;
    @BindView( R.id.btnSonido )
    ConstraintLayout btnSonido;
    @BindView( R.id.btnEnojado )
    ConstraintLayout btnEnojado;

    @BindView( R.id.tvContento )
    TextView tvContento;
    @BindView( R.id.tvSonido )
    TextView tvSonido;
    @BindView( R.id.tvEnojado)
    TextView tvEnojado;

    @BindView( R.id.btnBT )
    ImageView btnBT;

    @Override
    @SuppressLint("ClickableViewAccessibility")
    protected void onCreate ( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_principal );
        ButterKnife.bind( this );

        // Se obtiene la referencia al vibrador del telefono
        vibrator = ( Vibrator ) getSystemService( Context.VIBRATOR_SERVICE );

        // Se coloca como máximo de la barra de velocidad 255
        sbVelocidad.setMax( 255 );
        sbVelocidad.setOnSeekBarChangeListener( new SeekBar.OnSeekBarChangeListener ()
        {
            @Override
            public void onProgressChanged ( SeekBar seekBar, int i, boolean b )
            {
                // Al cambiar la barra, se setea esta velocidad
                cambiarVelocidad( i );
            }

            @Override
            public void onStartTrackingTouch ( SeekBar seekBar ) { }

            @Override
            public void onStopTrackingTouch ( SeekBar seekBar ) { }
        } );

        // Se agregan los listeners apropiados, y se pasa la información de qué comando se utilizará para cada listener
        btnAdelante.setOnTouchListener( new BotonListener( "adelante" ) );
        btnAtras.setOnTouchListener( new BotonListener( "atras" ) );
        btnIzquierda.setOnTouchListener( new BotonListener( "izquierda" ) );
        btnDerecha.setOnTouchListener( new BotonListener( "derecha" ) );

        btnAdelanteDer.setOnTouchListener( new BotonListener( "adelanteDer" ) );
        btnAdelanteIzq.setOnTouchListener( new BotonListener( "adelanteIzq" ) );
        btnAtrasDer.setOnTouchListener( new BotonListener( "atrasDer" ) );
        btnAtrasIzq.setOnTouchListener( new BotonListener( "atrasIzq" ) );

        btnGirarDer.setOnTouchListener( new BotonListener( "girarDer" ) );
        btnGirarIzq.setOnTouchListener( new BotonListener( "girarIzq" ) );

        btnFeliz.setOnTouchListener( new OpcionTextoListener( tvContento, "bender", 0, true ) );
        btnSonido.setOnTouchListener( new OpcionTextoListener( tvSonido, "bender", 2, false ) );
        btnEnojado.setOnTouchListener( new OpcionTextoListener( tvEnojado, "bender", 1, false ) );

        btnBT.setOnClickListener( new View.OnClickListener ()
        {
            @Override
            public void onClick ( View view )
            {
                boton_bt();
            }
        } );
    }

    private String comando;
    private void cambiarComando ( String comando )
    {
        // Se cambia globalmente el comando
        this.comando = comando;
        // Se envía el comando
        enviarComando();
    }

    private int velocidad;
    private void cambiarVelocidad ( int velocidad )
    {
        // Se cambia globalmente la velocidad
        this.velocidad = velocidad;
        // Se envía el comando
        enviarComando();
    }

    private void enviarComando ()
    {
        // Se envía el comando concatenado de la velocidad
        enviarComando( String.format( Locale.getDefault(), "%s:%d\n", comando, velocidad ) );
    }

    // =============================================================================================    Bluetooth

    private static final String BT_NAME = "HC-05";
    private static final UUID BT_UUID = UUID.fromString( "00001101-0000-1000-8000-00805F9B34FB" );
    private static final int BT_ACTIVAR = 1;

    private BluetoothAdapter btAdapter = null;
    private BluetoothSocket btSocket = null;
    private boolean conexiónEstablecida = false;
    private boolean conectando = false;

    private boolean receiverRegistrado = false;
    private final BroadcastReceiver receiver = new BroadcastReceiver ()
    {
        @Override
        public void onReceive ( Context context, Intent intent )
        {
            final String accion = intent.getAction();

            // Si por cualquier razón se desconecta el bluetooth, se llama a bt_desconectado_inesperado
            assert accion != null;
            if ( accion.equals( BluetoothDevice.ACTION_ACL_DISCONNECTED ) )
                bt_desconectado_inesperado();
            else if ( accion.equals( BluetoothAdapter.ACTION_CONNECTION_STATE_CHANGED ) )
                if ( intent.getIntExtra( BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR ) == BluetoothAdapter.STATE_OFF )
                    bt_desconectado_inesperado();
        }
    };

    // Cuando el botón del bluetooth es presionado
    private void boton_bt ()
    {
        // Si ya se está intentando conectar, no hacer nada
        if ( conectando )
            return;

        // Si ya está conectado, entonces se desconecta
        if ( conexiónEstablecida )
            bt_desconectar();
        else // De lo contrario se pregunta para conectar al bluetooth
            preguntar_para_conectar_bt();
    }

    private void preguntar_para_conectar_bt ()
    {
        btAdapter = BluetoothAdapter.getDefaultAdapter();

        // Si btAdapter es null, el dispositivo no cuenta con bluetooth integrado
        if ( btAdapter == null )
            Toast.makeText( this, "Este dispositivo no cuenta con conexión bluetooth", Toast.LENGTH_LONG ).show();
        else if ( btAdapter.isEnabled() ) // Si el bluetooth ya está activo en el celular, se llama a la función para conectar con el robot
            bt_conectar();
        else // De lo contrario se indica que es necesaria su activación
            new AlertDialog.Builder( this )
                .setIcon( android.R.drawable.ic_dialog_info )
                .setTitle( "Bluetooth" )
                .setMessage( "Es necesario encender el bluetooth" )
                .setPositiveButton( "Ok", new DialogInterface.OnClickListener ()
                {
                    @Override
                    public void onClick ( DialogInterface dialogInterface, int i )
                    {
                        // Aquí es donde se muestra el cartel de permitir o rechazar
                        // Una vez seleccione uno esto llegará al "onActivityResult"
                        startActivityForResult( new Intent( BluetoothAdapter.ACTION_REQUEST_ENABLE ), BT_ACTIVAR );
                    }
                } )
            .show();
    }

    @Override
    protected void onActivityResult ( int requestCode, int resultCode, @Nullable Intent data )
    {
        super.onActivityResult( requestCode, resultCode, data );

        if ( requestCode == BT_ACTIVAR )
        {
            // Si aceptó activar el bluetooth se llama a la función para conectar con el robot
            if ( resultCode == RESULT_OK )
                bt_conectar();
            else if ( resultCode == RESULT_CANCELED ) // De lo contrario se muestra un mensaje que reitera que es necesario encender el bluetooth
                Toast.makeText( this, "Es necesario encender el bluetooth", Toast.LENGTH_LONG ).show();
        }
    }

    @SuppressLint( "StaticFieldLeak" )
    private void bt_conectar ()
    {
        // Se obtienene todos los dispositivos bluetooth vinculados
        Set<BluetoothDevice> dispositivos = btAdapter.getBondedDevices();

        if ( dispositivos.size() == 0 ) // Si no lo hay se indica que se vincule al BT_NAME
            Toast.makeText( this, "Vincule con el dispositivo " + BT_NAME, Toast.LENGTH_LONG ).show();
        else
        {
            // Si hay dispositivos en la lista, se intentará encontrar el BT_NAME
            BluetoothDevice dispositivo = null;

            for ( BluetoothDevice disp : dispositivos )
                if ( disp.getName().equals( BT_NAME ) )
                {
                    dispositivo = disp;
                    break;
                }

            if ( dispositivo == null ) // Si no se encontró,se indica que se vincule al BT_NAME
                Toast.makeText( this, "Vincule con el dispositivo " + BT_NAME, Toast.LENGTH_LONG ).show();
            else
            {
                new AsyncTask<BluetoothDevice, Void, Void> ()
                {
                    private boolean conectado = true;

                    @Override
                    protected void onPreExecute ()
                    {
                        // Antes de iniciar la conexión, se cambia el icono del botón y el color para indicar que se está conectando
                        btnBT.setImageResource( R.drawable.ic_bluetooth_conectando );
                        btnBT.setColorFilter( getResources().getColor( R.color.conectando ) );
                        conectando = true;
                    }

                    @Override
                    protected Void doInBackground ( BluetoothDevice... dispositivo )
                    {
                        try
                        {
                            btSocket = dispositivo[0].createInsecureRfcommSocketToServiceRecord( BT_UUID );
                            btAdapter.cancelDiscovery();
                            btSocket.connect();
                        }
                        catch ( IOException e ) { conectado = false; }

                        return null;
                    }

                    @Override
                    protected void onPostExecute ( Void resultado )
                    {
                        // Al terminar de intentar la conexión
                        conectando = false;

                        // Si no se conectó
                        if ( !conectado )
                        {
                            Toast.makeText( Principal.this, "No se pudo establecer la conexión con bender", Toast.LENGTH_LONG ).show();

                            btnBT.setImageResource( R.drawable.ic_bluetooth );
                            btnBT.setColorFilter( getResources().getColor( R.color.desconectado ) );
                        }
                        else
                            conexion_establecida();
                    }
                }.execute( dispositivo ); // Se ejecuta esta tarea asincrona, enviando el dispositivo
            }
        }
    }

    private void conexion_establecida ()
    {
        // Se registra el broadcast del bluetooth, para saber cuando se desconecte el robot
        // O se apague el bluetooth
        registerReceiver( receiver, new IntentFilter( BluetoothDevice.ACTION_ACL_DISCONNECTED ) );
        registerReceiver( receiver, new IntentFilter( BluetoothAdapter.ACTION_STATE_CHANGED ) );

        receiverRegistrado = true; // Necesario para el onDestroy()
        conexiónEstablecida = true; // Se marca la conexión como establecida

        // Se cambia el ícono del bluetooth para reflejar la conexión
        btnBT.setImageResource( R.drawable.ic_bluetooth_conectado );
        btnBT.setColorFilter( getResources().getColor( R.color.conectado ) );
    }

    private void enviarComando ( String comando )
    {
        Log.i( TAG, comando );
        // Se envía el mensaje a través de bluetooth
        try
        {
            if ( btSocket != null )
                btSocket.getOutputStream().write( comando.getBytes() );
        }
        catch ( IOException ignored ) {}
    }

    private void bt_desconectado_inesperado ()
    {
        bt_desconectar();
    }

    private void bt_desconectar ()
    {
        // Si se registró el receiver, se quita el registro
        if ( receiverRegistrado ) { unregisterReceiver( receiver ); receiverRegistrado = false; }

        // Se desconecta el socket de la comunicación
        if ( btSocket != null )
        {
            try { btSocket.close(); }
            catch ( IOException ignored ) {}
        }

        btSocket = null;

        conexiónEstablecida = false; // Se marca que no hay conexión
        // Se la visual del botón de bluetooth para que refleje el estado
        btnBT.setImageResource( R.drawable.ic_bluetooth );
        btnBT.setColorFilter( getResources().getColor( R.color.desconectado ) );
    }

    @Override
    protected void onDestroy ()
    {
        super.onDestroy();

        if ( receiverRegistrado ) { unregisterReceiver( receiver ); receiverRegistrado = false; }
        if ( conexiónEstablecida ) bt_desconectar(); // En caso de que la aplicación se cierre con la conexión establecida, se llama a desconectar
    }
}
